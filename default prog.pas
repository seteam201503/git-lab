﻿uses crt;
const n=7;
var 
    a:array[1..n,1..n] of integer;
    i,j,k:integer;
begin
clrscr;
randomize;
writeln('Original matrix: ');
k:=0;
for i:=1 to n do
    begin
    for j:=1 to n do
        begin
            a[i,j]:=-14+random(27);
            write(a[i,j]:4);
            if (i=j)and(a[i,j]<0) then 
            inc(k);
        end;
    writeln;
    end;
writeln('Number of negative numbers of main diognal: ',k);
writeln('Changed matrix: ');
k:=0;
for i:=1 to n do
    begin
        for j:=1 to n do
            begin
            if (j<n-i+1) and (a[i,j]>0) then 
            a[i,j]:=0;
            write(a[i,j]:4);
            end;
        writeln;
    end;
    readln
end.